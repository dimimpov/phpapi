<?php

namespace App\Controller;

use App\Entity\User;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validation;

/**
 * Auth Controller
 * @Route("api/auth")
 */
class AuthController extends AbstractFOSRestController
{
    /**
     * Create a User
     * @Rest\Post("/register",name="register_a_user")
     *
     * @param Request $request
     * @param UserManagerInterface $userManager
     * @return JsonResponse
     */
    public function register(Request $request, UserManagerInterface $userManager)
    {
        $data = json_decode($request->getContent(), true);
        $validator = Validation::createValidator();
        $constraint = new Assert\Collection(array(
            // the keys correspond to the keys in the input array
            'username' => new Assert\Length(array('min' => 1)),
            'password' => new Assert\Length(array('min' => 1)),
            'email' => new Assert\Email(),
        ));
        $violations = $validator->validate($data, $constraint);

        if ($violations->count() > 0) {
            return new JsonResponse(["error" => (string) $violations], 500);
        }
        $username = $data['username'];
        $password = $data['password'];
        $email = $data['email'];

        $user = $userManager->createUser();

        $user
            ->setUsername($username)
            ->setPlainPassword($password)
            ->setEmail($email)
            ->setEnabled(true)
            ->setRoles(['ROLE_USER'])
            ->setSuperAdmin(false)
            ->setisActive(true)
        ;
        try {
            $userManager->updateUser($user);

        } catch (\Exception $e) {
            return new JsonResponse(["error" => $e->getMessage()], 500);
        }
        return new JsonResponse(["success" => $user->getUsername() . " has been registered!"], 200);
    }

}
