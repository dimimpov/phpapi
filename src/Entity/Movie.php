<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="movie")
 */
class Movie
{
    /**
     * @ORM\Column(type="integer")
     * *@ORM\Id
     * *@ORM\GeneratedValue(strategy="AUTO")
     * */
    private $id;

    /**
     *@ORM\Column(type="string",length=100)
     * @Assert\NotBlank()
     **/
    private $name;

    /**
     *@ORM\Column(type="string")
     * @Assert\NotBlank()
     **/
    private $description;

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }
}
